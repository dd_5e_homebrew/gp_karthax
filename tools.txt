to extract text and images from a pdf in preparation for conversion

open windows terminal and ubuntu 20.04

use the tool pdfimages (part of the poppler-utils package)

pdfimages -png <pdf-file> <image root>

image root is a directory (./images) and a root file name (img)

pdfimages -png The_Lost_Vaults.pdf source/img

cull art files to needed files only.

The following command will rename them into sequential files (if they are png files)

 ls -v *.png | cat -n | while read n f ; do num=$(printf "%03d" "${n}"); mv -n "$f" "image-${num}.png"; done

once culled and sequential, use the process_images.sh script to combine alpha and image and rename properly into images directory

if images come in inverted they too can be batch processed:

ls -v img-*.jpg |cat -n | while read n f; do num=$(printf "%05d" "${n}"); convert "$f" -negate -profile /mnt/c/Users/ronin/Pictures/ICC_Profiles/CMYK/USWebCoatedSWOP.icc image-${num}.jpg; done

Text Processing:

extract text of 2 column pdf with half inch margins
(Using Powershell)
convert-pdftotext pdfname.pdf
clean up resulting text by converting unicode characters to their FGU legal iso8859-1 equivalents
convert-fgbadchars pdfname.txt

steps in order 
1. search and replace remaining illegal characters:
/[^\d0-\d127] - search for all non ascii characters
manually replace with iso-8859-1 equivalent and potentially add to convert-fgbbadchars.


%s/^\d\+$//g - get rid of digits alone on a line (page numbers and table entry numbers)
%s/^\n\(\p\)\@!// - get rid of duplicate empty lines
%s/^\n// - remove extra  characters
get rid of pdf watermark 
search for tables and add #table# tag

%s/\(\%>40v\p\)\([\.?!:"]\)\@<!\n\(\n\)\@!/\1 \2/c - join lines of paragraphs

%s/\(\*\)\@<!\(\*\*\)\(\*\)\@!\(\p\{-1,}\)\(\*\*\)/{@b \4}/gc - replace markdown ** with {@b }

%s/\(DC\)\s\(\d\+\)\s\(\w\+\)\s\([^(]\)/{@b {@dc \2} \3} \4/gc - format saves with bold and @dc type

%s/\(DC\)\s\(\d\+\)\s\(\w\+\)\s\((\p\+)\)/{@b {@dc \2} \3 \4}/gc - format skill checks with bold and @dc type

%s/\( or\) \(\a\{-,11}\) \((\a\{-,11})\)/\1 {@b \2 \3}/gc - surround or clause of skill check with {@b }

%s/\(\*\)\@<!\(\*\)\(\*\)\@!\(\p\{-1,}\)\(\*\)/{@i \4}/gc - replace markdown * with {@i }

%s/\(<u>\)\(\p\{-1,}\)\(<\/u>\)/{@u \2}/gc - replace <u>  with {@u }

find item type and rarity, break line, add cost line
%s/^\(\p+\)\(\s\p\+\)\?,\s\([Cc]ommon\|[Uu]ncommon\|[Rr]are\|[Vv]ery\s[Rr]are\|[Ll]egendary\)\(\s(\p\+)\)\?/\1\2\r\3\4\rCost:/c

%s/\s\+$//e - remove trailing whitespace from lines

:%s/&gt;/>/g  - replace entity &gt; with >

:%s/&lt;/</g  - replace entity &lt; with <

g/^$/d - remove empty lines 

\(<\p\{-}>\)\([^<]\p\+\)\(<\/\p\{-}>\)/\2/ - keep contents of an html tag pair

Screen Captures
use sizer (Ctrl+win+z -> FG screencap) to resize FG
/scaleui 75
Alt+prtscn to capture the image
Open screencap directory (C:\Users\ronin\Pictures\Screenshots)
magick convert .\filename.png -crop 1000x563+0+48 outputfilename.jpg
repeat as necessary


Misc Notes:

Capitalize first letter of wondrous item and very rare
%s/\(w\)\(ondrous\)\(\s\)\(i\)\(tem\)/\u\1\L\2\3\u\4\L\5/c
%s/\(v\)\(ery\)\(\s\)\(r\)\(are\)/\u\1\L\2\3\u\4\L\5/c

Vim commands
~ - captialize the character under the cursor
dat - delete around tags (inclusive)
yat - yank around tags (inclusive)
dit - delete inside tags
cit - change inside tags
y$ - yank from cursor to end of line
d$ - delete from cursor including end of line
 

